# ProtVista plugin

The *protvista* plugin provides residue-level annotations for UniProt annotated genes via the 
[ProtVista](https://github.com/ebi-webcomponents/protvista-uniprot/) tool.

*Please note that this plugin is not really production ready (see the last section)*


### General instructions

In order for the plugin to work, the map needs to be annotated by UniProt, i.e. the residue-level
annotations will show only for genes which have UniProt annotations.

![printscreen](img/plugin.gif)

### Plugin functionality

When the plugin is uploaded into MINERVA, it shows an empty space which is populated by the ProtVista
tool once a gene with a UniProt annotation is selected. If the selected gene contains also variation
amino acid-level variation information this is then shown in ProtVista in the Variants panel.

### "Issues" & TODO

This plugin was created as a part of European BioHackathon2021 and is still a prototype 
due to several reasons which would need to be addressed to have a production-ready versions.

- Development of the plugin required modification of the ProtVista code, so it currently uses a
[fork of ProtVista](https://github.com/davidhoksza/protvista-uniprot). This fork includes the 
following list of changes:
  - Passing user-level annotations (variation information in case of this plugin) as a part
of the config object. Due to a bug in ProtVista it was not possible to pass multiple
track data object (although that object is an array - see the `config.json`). The
hack is to take the API address from the config, run the respective API call, 
merge that with the user data and pass this as the `payload` field of a single data object to ProtVista.
  - ProtVista is typed and types of data sources and variant consequences are in the current version
of ProtVista basically enums meaning they are not dynamically populated. To be able to show custom 
variations, the forked version of ProtVista extends the possible variation types 
by user variation type (`filterConfig.ts - name: 'USER'`). For a variation 
to show as "User provided" item in `Filter provenance` 
it needs to have `userProvided` field set to true. But this is really just a dirty hack as
there is no way to differentiate different sources of user-provided variations. 
  - The forked version of ProtVista contains bundled d3.js. This is becuase the plugin is loaded on 
the fly and when ProtVista library loads (even before the component is created) it counts on having
`d3` in the global space. However, if `d3` is loaded in the plugin just before ProtVista, it does 
not exist in the global space yet and ProtVista will fail. To be able to remove d3 from the ProtVista
bundle, the plugin would have to somehow synchronously load d3 so that it was available at the moment
when ProtVista loads.
- It is not possible to have custom variation consequences and currently all of them show in ProtVista
as *Uncertain*.
- The code needs to be minified as the resulting bundle is too big (this is also due to the inclusion
of the d3 library (see above))
- ProtVista is currently included as asset JS file. This should be changed to dependency, but
during the BioHackathon was not enough time to find out how correctly setup webpack to achieve that. 
- The current code supports variation fetching from the map only for MINERVA v16. The code needs to
be slightly modified to support both MINERVA versions <v16 and >=v16.
