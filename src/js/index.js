// import 'd3'
// console.log('d3', d3);
// window.d3 = d3;
import {retrieveVariants} from './variants'
import '../assets/protvista-uniprot/protvista-uniprot';
// import ProtvistaUniprot from 'protvista-uniprot';
// import '../assets/uniprot-nightingale';


import "../css/styles.scss"
import config from "../config.json"


const pluginName = 'protvista';
const pluginVersion = '1.0.0';

const containerName = pluginName + '-container';

const globals = {
    selected: [],
    allBioEntities: [],
    pickedRandomly: undefined
};

let $ = window.$;
if ($ === undefined && minerva.$ !== undefined) {
    $ = minerva.$;
}

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;
let minervaVersion;
let protvistaContainer;
let protvistaModalContainer;
let protvistaModal;

const register = function(_minerva) {

    console.log('Registering ' + pluginName + ' plugin');

    // window.customElements.define('protvista-uniprot', ProtvistaUniprot);


    $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);
    pluginContainerId = pluginContainer.attr('id');
    if (!pluginContainerId) {
        //the structure of plugin was changed at some point and additional div was added which is the container but does not have any properties (id or height)
        pluginContainerId = pluginContainer.parent().attr('id');
    }


    console.log('minerva object ', minervaProxy);
    console.log('project id: ', minervaProxy.project.data.getProjectId());
    console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    return minerva.ServerConnector.getConfiguration().then( function(conf) {
        minervaVersion = parseFloat(conf.getVersion().split('.').slice(0, 2).join('.'));
        console.log('minerva version: ', minervaVersion);
        initPlugin();
    });
};

const unregister = function () {
    console.log('unregistering ' + pluginName + ' plugin');

    unregisterListeners();
    return deHighlightAll();
};

const getName = function() {
    return pluginName;
};

const getVersion = function() {
    return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion,
        minWidth: 800,
        defaultWidth: 800
    }
});

function initPlugin () {
    registerListeners();
    initMainPageStructure();
    pluginContainer.find(`.${containerName}`).data("minervaProxy", minervaProxy);
}

function registerListeners(){
    minervaProxy.project.map.addListener({
        dbOverlayName: "search",
        type: "onSearch",
        callback: searchListener
    });
}

function unregisterListeners() {
    minervaProxy.project.map.removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************


function deHighlightAll(){
    return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

function queryMinervaApi(query, body="", method="GET") {

    const apiAddress = ServerConnector.getApiBaseUrl();

    return fetch(`${apiAddress}/${query}`, {
        method: method,
        body: body
    })
}


// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function initMainPageStructure(){
    console.log("pluginContainer", pluginContainer);
    protvistaContainer = $(`<div class="${pluginName}-protvista"></div>`)
    pluginContainer.append(`
        <div class="modal fade" id="protvistaModal" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div id="protvistaModalContainer" class="modal-body">            
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="${pluginName}-protvista-header">
            <div class="container">
                <div class="row">
                    <div class="col col-md-3">
                            <span class="${pluginName}-uniprot-id-label">UniProt ID: </span>
                    </div>
                    <div class="col col-md-7">
                        <span class="${pluginName}-uniprot-id-value"></span>
                    </div>
                    <div class="col col-md-2">
                        <button type="button" class="btn btn-warning"style="width: 100%;" onclick="$('#protvistaModal').modal('show')">Fullscreen</button>
                    </div>                        
                </div>
            </div>
        </div>
        <div class="${pluginName}-header">
        </div>`);
    pluginContainer.append(protvistaContainer);
    protvistaModalContainer = pluginContainer.find("#protvistaModalContainer");
    protvistaModal = pluginContainer.find("#protvistaModal");
    console.log('protvistaModal', protvistaModal);

    protvistaModal.on('show.bs.modal', function (event) {
        $("#protvistaWrapper").detach().appendTo(protvistaModalContainer);
    })

    protvistaModal.on('hide.bs.modal', function (event) {
        $("#protvistaWrapper").detach().appendTo(protvistaContainer);
    })
}

async function appendVariants(config, uniprotId, elementId){
    const overlaysVariants = await retrieveVariants(minervaProxy.project.map, elementId);
    console.log('retrieved variants', overlaysVariants);

    let ixVar;
    config.categories.forEach((d, i) => {if (d.name === 'VARIATION') ixVar = i });
    const varAPI = config.categories[ixVar].tracks[0].data[0].url;
    await fetch(varAPI.replace('{accession}', uniprotId))
        .then(res => res.json())
        .then(res => {
            res.features = res.features.concat(overlaysVariants.map(overlayVariants => overlayVariants.variants).flat());
            config.categories[ixVar].tracks[0].data[0].url = 'custom-variants';
            config.categories[ixVar].tracks[0].data[0].payload = res;
        })

    return config;
}

async function searchListener(entities) {
    console.log("entities", entities)
    if (entities[0].length !== 1) return; //if there are more entities, the user clicked a reaction
    const entity = entities[0][0];
    const uniProtIds = entity.getReferences().filter(d => d.getType()==='UNIPROT');
    if (uniProtIds.length === 0) return; //there is not UniProt ID for that record
    const uniprotId = uniProtIds[0].getResource();
    protvistaContainer.empty();
    pluginContainer.find(`.${pluginName}-uniprot-id-value`).text(uniprotId);

    const configMod = JSON.parse(JSON.stringify(config)); // slow deep copy
    await appendVariants(configMod, uniprotId, entity.id);

    console.log('modified config', configMod);

    protvistaContainer.append(`
        <div id="protvistaWrapper" style="position: relative">
            <protvista-uniprot id="pvContainer" accession="${uniprotId}" config='${JSON.stringify(configMod)}'/>
        <div>
    `);
}


